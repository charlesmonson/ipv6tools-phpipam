#!/usr/bin/python3

import phpipamapi as ipamapi
from random import randint

def main():
    print('-' * 80)
    if ipamapi.check_auth_token():
        token = ipamapi.get_auth_token('yes')
        #print(f'Token exists!')
    else:
        token = ipamapi.get_auth_token('no')
        #print(f'Success! Token has been set to {token}')

    circuit_id = f'{randint(100000, 999999)}-CTID'

    v6net = ipamapi.get_free_static(ipamapi.apiurl, ipamapi.config.static_netid, ipamapi.config.static_prefix_increment, token)
    p2pnet = ipamapi.generate_ptp_from_static(str(v6net.network_address) + '/' + ipamapi.config.static_prefix_length)

    response_ptp = ipamapi.assign_p2p(circuit_id, str(p2pnet.network_address), token)

    if response_ptp.status_code == 409:
        response_static = ipamapi.assign_static('autogen-ERROR', str(v6net.network_address), token)
        print(f'There was an error assigning {v6net.with_prefixlen}({p2pnet.with_prefixlen}). NO ADDRESS WAS ASSIGNED! Please run script again.')
        quit()
    elif response_ptp.status_code == 201:
        response_static = ipamapi.assign_static(circuit_id, str(v6net.network_address), token)

    #print('-' * 80)
    #print(response_static.text)
    #print(response_ptp.text)
    print('-' * 80)

    print(f'Static Subnet (ID:{response_static.json()["id"]}): {response_static.json()["data"]:>23}')
    print(f'P2P Subnet    (ID:{response_ptp.json()["id"]}): {response_ptp.json()["data"]:>28}')
    print('-' * 80)

if __name__ == '__main__':
    main()
