#!/usr/bin/python3

import requests
import json
import getpass
import ipaddress

try:
    import config
except ModuleNotFoundError:
    print("Could not import config. You must copy 'example-config.py' to 'config.py' and edit the variables for your environment.")
    exit()

#Constants

apiurl = config.phpipam_hosturl + config.phpipam_appname

def check_auth_token():
    try:
        token = open('./tokenfile', 'r').read()
        token = token.strip()

        header_token = {'token': token}
        r = requests.get(apiurl + '/user/', headers=header_token)
        if r.status_code == 200:
            return True
        elif r.status_code == 403:
            print("We got a 403 error.")
            return False
        else:
            print("Unknown error.")
            print(r.text)
            return False
        token.close()

    except FileNotFoundError:
        print('Token does not exist.')
        return False

def get_auth_token(auth_exist):
    if auth_exist is 'no':
        try:
            user = config.user
            password = config.password
        except:
            user = input("You need authorization.\nPlease enter a username: ")
            password = getpass.getpass()

        r = requests.post(apiurl + '/user/', auth=(user, password))
        token = r.json()["data"]["token"]

        with open('./tokenfile', 'w') as tokenfile:
            tokenfile.write(token)

        return token

    elif auth_exist is 'yes':
        with open('./tokenfile', 'r') as tokenfile:
            token = tokenfile.read().strip()
        return token

def list_networks():
    pass
def get_location():
    pass

def get_free_static(url, netid, increment, token):
    try:
        header_token = {'token': token}
        r = requests.get('{}/subnets/{}/first_subnet/{}/'.format(url, netid, increment), headers=header_token)
        #print(r.text)
        v6net = ipaddress.IPv6Network(str(r.json()["data"]))
        return v6net

    except:
        print('Something went wrong with get_free_static()')

def generate_ptp_from_static(base_v6net):
    v6net = ipaddress.IPv6Network(base_v6net)
    explode_net = v6net.exploded
    temp_net = explode_net.split('/')
    temp_net = temp_net[0]

    split_net = temp_net.split(':')
    hextet = split_net[2]
    split_net[2] = 'ffff'
    split_net[3] = hextet

    join_net = ':'.join(split_net)
    p2p_net = ipaddress.IPv6Network(join_net + '/64')
    return p2p_net

def assign_static(description, network, token):
    header_token = {'token': token}
    payload = {
            'description':'autogen_' + description,
            'sectionId':config.phpipam_sectionid,
            'masterSubnetId':config.static_netid,
            'subnet':network,
            'mask':config.static_prefix_length
            }
    r = requests.post(apiurl + '/subnets/', json=payload, headers=header_token)
    return r

def assign_p2p(description, network, token):
    header_token = {'token': token}
    payload = {
            'description':'autogen-p2p_' + description,
            'sectionId':config.phpipam_sectionid,
            'masterSubnetId':config.ptp_netid,
            'subnet':network,
            'mask':config.ptp_prefix_length
            }
    r = requests.post(apiurl + '/subnets/', json=payload, headers=header_token)
    return r

def check_base_static(v6net):
    pass
def check_p2p(p2pnet):
    # Use output from generate_ptp(). Check PHPIPAM for subnet to see if it is free. 
    pass

def update_network():
    pass


if __name__ == '__main__':
    if check_auth_token():
        token = get_auth_token('yes')
        print("Token already existed: " + token)
    else:
        token = get_auth_token('no')
        print("Success! Token is now " + str(token))

    v6net = get_free_static(apiurl, config.static_netid, config.static_prefix_increment, token)
    v6net = ipaddress.IPv6Network(str(v6net.network_address) + '/' + config.static_prefix_length)
    ptpnet = generate_ptp_from_static(v6net)
    print('-' * 80)
    print(v6net.with_prefixlen)
    print(ptpnet.with_prefixlen)
