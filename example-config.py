## Put server/hostname variables here

phpipam_hosturl = 'https://localhost/api/'
phpipam_appname = 'python'
phpipam_sectionid = '2'

## ID of static network blocks

static_netid = '8' # This is the network where 'base' static assignments will be made. Tested with a /36. Example of 2001:db8:f000::/36.
ptp_netid = '9' # This is the network where ptp networks will be assigned. The script assumes this to be the last /48 in the above /36 (ffff).

## Subnet length settings

static_prefix_length = '48'
static_prefix_increment = '44' # Set a lower number than 'static_prefix_length' here to leave gaps between assignments
ptp_prefix_length = '64'

## Authentication
### Script will prompt for credentials if these are not defined
### It is more secure to run it without these variables defined, but less convenient

# user = 'username'
# password = 'password'
