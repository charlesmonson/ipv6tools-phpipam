#!/usr/bin/python3

import requests
import json
import getpass
import ipaddress
from sys import argv
import phpipamapi as ipamapi
import re

#script,token = argv
if ipamapi.check_auth_token():
    with open('./tokenfile', 'r') as tokenfile:
        token = tokenfile.read().strip()
else:
    print("Auth token is expired or missing")

apiurl = ipamapi.apiurl

header_token = {'token': token}
header_test = requests.get(apiurl + '/user/', headers=header_token)

v6subnets = requests.get(apiurl + '/sections/' + ipamapi.config.phpipam_sectionid + '/subnets/', headers=header_token)

for net in v6subnets.json()["data"]:
    try:
        if re.match(r'autogen\_', net["description"]):
            print('DELETED: {}/{} - {} (ID: {})'.format(net['subnet'], net['mask'], net['description'], net['id']))
            requests.delete(apiurl + '/subnets/' + net['id'] + '/', headers=header_token)
        elif re.match(r'autogen-p2p\_', net["description"]):
            print('DELETED P2P: {}/{} - {} (ID: {})'.format(net['subnet'], net['mask'], net['description'], net['id']))
            requests.delete(apiurl + '/subnets/' + net['id'] + '/', headers=header_token)
        else:
            print('Skipped: ' + net['subnet'] + '/' + net['mask'] + ' - ' + net['description'])
    except TypeError:
        continue
